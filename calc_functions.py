# functions for credit_calculator.py module

import sys


def input_data():
    """ 
    Input data in format:
        amount: XXXX
        interest: XX.XX%
        downpayment: XXXX
        term: XXXX
    """

    while True:
        print("Input data.. (for end press ctrl+z and enter)")
        in_str = sys.stdin.read()
        if not check_input(in_str):
            break

    return in_str


def check_input(in_str):
    """ Check input is correct """
    
    str_elems = in_str.split()

    # check #1 = check keywords exists & values format is correct
    if (len(str_elems) < 8 or
        (str_elems[0] != 'amount:' or 
        str_elems[2] != 'interest:' or
        str_elems[4] != 'downpayment:' or
        str_elems[6] != 'term:'
       ) or 
        (not str_elems[1].isnumeric() or
         not str_elems[5].isnumeric() or
         not str_elems[7].isnumeric()) or
         str_elems[3][-1]!='%'
        ) or check_interest(str_elems[3][:-1]):
        print("Incorrect!")
        print("""
        Correct input format:
        amount: XXXX
        interest: XX.XX%
        downpayment: XXXX
        term: XXXX
        """)
        print("Try again...\n")
        return 1
    
    return 0  


def check_interest(i):
    """ check interest is float """
    try:
        float(i)  
    except ValueError:
        return 1
    
    return 0


def parse_params(in_str):
    """ Parse parameters from input string """
    str_elems = in_str.split()
    
    amount = int(str_elems[1])
    interest = float(str_elems[3][:-1]) / 100
    downpayment = int(str_elems[5])
    term = int(str_elems[7])
    
    return amount, interest, downpayment, term


def calculations(amount, interest, downpayment, term):
    """ Calculations of :
        - payment_month
        - total_payment_amount
        - total_interest_payment
    """
    
    # calculate sum of credit and cast to monthly basis
    credit_sum = amount - downpayment
    term_month = term * 12
    interest_month = interest / 12
    #print(credit_sum, term_month, interest_month)
    
    # month payment calculate (annity)
    coef = (interest_month * (1+interest_month)**term_month) / ((1+interest_month)**term_month - 1)
    payment_month = round(credit_sum * coef, 2)
    #print(coef, "\nmonth payment (annuity) =", payment_month)

    # total payment amount & interest payment
    total_payment_amount = round(payment_month * term_month, 2)
    #print(total_payment_amount)
    total_interest_payment = round(total_payment_amount - credit_sum, 2)
    #print(total_interest_payment)
    
    return payment_month, total_interest_payment, total_payment_amount


def display_result(payment_month, total_interest_payment, total_payment_amount):
    """ Display results of calculations """
    print("Месячная выплату по кредиту:", payment_month) 
    print("Общий объём начисленных процентов:", total_interest_payment)
    print("Общая сумма выплаты:", total_payment_amount)


def convert_data_to_json(amount, interest, downpayment, term, 
                    payment_month, total_interest_payment, total_payment_amount):
    """ Convert results of calculations to json for further handling/sending """
    import json
    
    data = {
    "amount": amount,
    "interest": interest,
    "downpayment": downpayment,
    "term": term,
    "payment_month": payment_month,
    "total_interest_payment": total_interest_payment,
    "total_payment_amount": total_payment_amount
    }
    json_string = json.dumps(data)

    return json_string
