# this module realize credit calculator functions
# all user definted functions are in calc_function.py module

import sys
from calc_functions import input_data, check_input, check_interest, parse_params, calculations, display_result, convert_data_to_json


# step #1 = data input
in_str = input_data()

# step #2 = check input data
check_input(in_str)

# step #3 = parse & assign parameters
amount, interest, downpayment, term = parse_params(in_str)

# step #4 = calculations
payment_month, total_interest_payment, total_payment_amount = calculations(amount, interest, downpayment, term)

# step #5 = display results
display_result(payment_month, total_interest_payment, total_payment_amount)

# step #6 = additional function:  convertion data to json (for further handling)
convert_data_to_json(amount, interest, downpayment, term, 
                    payment_month, total_interest_payment, total_payment_amount)
